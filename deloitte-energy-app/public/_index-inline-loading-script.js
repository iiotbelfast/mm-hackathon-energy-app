// Below is a fast app loading script
// The script async & lazy-loads webcomponents-lite polyfill
// then checks for Polymer element
// then removes #splash div to reveal application UI in #app
(function() {

  // There following four lines are entered here as they are too late in the process to take effect
  // See: https://forum.predix.io/questions/28700/how-to-include-px-vis-worker-in-the-predix-webapp.html
  // See: https://github.com/PredixDev/predix-webapp-starter/issues/18
  // set some global variables for px-vis webworkers.
  window.Px = window.Px || {};
  window.Px.vis = window.Px.vis || {};
  window.Px.vis.workerUrl = '../../bower_components/px-vis/px-vis-worker.js';
  window.Px.vis.workerD3Url = '../../bower_components/pxd3/d3.min.js';

  // Wait for async loading of elements.html bundle
  var onWebComponentsLoaded = function() {
    var mainElementLink = document.querySelector('#main-element-import');
    if (mainElementLink.import && mainElementLink.import.readyState === 'complete') {
      onMainElementLoaded();
    } else {
      mainElementLink.addEventListener('load', onMainElementLoaded);
    }
  };

  // Remove #splash div and 'loading' class from body
  var onMainElementLoaded = function() {
    // Fade splash screen, then remove
    var splashEl = document.getElementById('splash');
    splashEl.parentNode.removeChild(splashEl);

    // The following is commented out. See top of file.
    // set some global variables for px-vis webworkers.
    // window.Px = window.Px || {};
    // window.Px.vis = window.Px.vis || {};
    // window.Px.vis.workerUrl = '../../bower_components/px-vis/px-vis-worker.js';
    // window.Px.vis.workerD3Url = '../../bower_components/pxd3/d3.min.js';
  };

  // load webcomponents polyfills
  if ('registerElement' in document && 'import' in document.createElement('link') && 'content' in
    document.createElement('template')) {
    // browser has web components, no need to load webcomponents polyfill
    onWebComponentsLoaded();
  } else {
    // polyfill web components
    var polyfill = document.createElement('script');
    polyfill.async = true;
    polyfill.src = '../../bower_components/webcomponentsjs/webcomponents-lite.min.js';
    polyfill.onload = onWebComponentsLoaded;
    document.head.appendChild(polyfill);
  }

}());
