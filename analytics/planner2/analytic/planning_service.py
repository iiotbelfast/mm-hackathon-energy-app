#!/usr/bin/python
import pandas as pd
import numpy as np
import time
import json

def time_to_epoch(timestamp):
    epoch = int(int(pd.Series(timestamp).to_json(orient='values')[1:-1])/1000)
    return epoch


def generate_forecast(start, end, supply_forecast):
    sfList = eval(supply_forecast)
    sfSeries = pd.Series(sfList)
    times = pd.date_range(start=start, end=end, freq='30T')
    hour_of_week = times.weekday*24 + times.hour
    forecast = sfSeries.iloc[hour_of_week].values
    result = pd.DataFrame({'timestamp': times.values, 'Supply': forecast}).set_index('timestamp')
    return result['Supply']


def calculate_cost(start, end, appliance, supply_forecast, demand_profile):
    forecast = generate_forecast(start, end, supply_forecast)
    demand = eval(demand_profile)
    window_size = len(demand)
    cost = forecast.rolling(window_size).agg(lambda x: x.dot(demand))
    cost = cost.reset_index()
    cost = cost[cost['Supply'].notnull()]
    cost['timestamp'] -= pd.Timedelta((window_size-1)*0.5, unit='h')
    cost = cost.set_index('timestamp')['Supply']
    return cost


def schedule_task(start, end, appliance):
    cost = calculate_cost(start, end, appliance)
    best_time = cost.argmin()
    return best_time


class PlanningService:

   def __init__(self):
      print("Hello from Planning Service")

   def schedule_task_service(self, data):
      data = json.loads(data)
      carbon_forecast = generate_forecast(data['start'], data['end'], data['supply_forecast']).rename('energy mix forecast')
      cost = calculate_cost(data['start'], data['end'], data['appliance'], data['supply_forecast'], data['demand_profile']).rename('predicted appliance emmissions')
      best_start = cost.argmin()
      result = pd.concat([carbon_forecast, cost], axis=1).to_json(date_unit='s', orient='split')
      result = json.loads(result)
      result[result['columns'][0]] = [x[0] for x in result['data']]
      result[result['columns'][1]] = [x[1] for x in result['data']]
      result['time_stamp'] = result['index']
      del result['data']
      del result['columns']
      del result['index']
      best_start = time_to_epoch(best_start)
      result = {'best_start': best_start, 'timeseries': result}
      return json.dumps(result)


