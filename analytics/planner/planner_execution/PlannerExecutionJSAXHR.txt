var data = JSON.stringify({
  "start": "2017-06-26 05:00:00",
  "end": "2017-06-28 05:00:00",
  "appliance": "iron"
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "https://predix-analytics-catalog-release.run.aws-usw02-pr.ice.predix.io/api/v1/catalog/analytics/fed2be02-e88b-4525-9b5c-0007b229e2da/execution");
xhr.setRequestHeader("content-type", "application/json");
xhr.setRequestHeader("authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6ImxlZ2FjeS10b2tlbi1rZXkiLCJ0eXAiOiJKV1QifQ.eyJqdGkiOiIyODhmNjljMWZiZTA0Zjg0OWMxM2E0MzUzOGI5NDhjOSIsInN1YiI6ImNsaWVudCIsInNjb3BlIjpbInRpbWVzZXJpZXMuem9uZXMuMWFkOGVmMTItM2NiNy00NGI3LWE0ZWItNjVjYzM4ZjFjNTgyLmluZ2VzdCIsInByZWRpeC1hc3NldC56b25lcy45NTIzY2E3Ni00ODgyLTQ5ZGEtYTNjMS0wMWRlN2M4MGZiZjMudXNlciIsInVhYS5yZXNvdXJjZSIsImludGVsbGlnZW50LW1hcHBpbmcuem9uZXMuNWJiYTgyYWItOTY4Yy00Yjc3LTlhM2UtODVjYmE2NzZhNDM2LnVzZXIiLCJvcGVuaWQiLCJ1YWEubm9uZSIsInRpbWVzZXJpZXMuem9uZXMuMWFkOGVmMTItM2NiNy00NGI3LWE0ZWItNjVjYzM4ZjFjNTgyLnF1ZXJ5IiwiYW5hbHl0aWNzLnpvbmVzLjExMGMxMTU3LWUzYTItNDhkMy1hZjNkLTkwNmYxMmIzMmZkZS51c2VyIiwidGltZXNlcmllcy56b25lcy4xYWQ4ZWYxMi0zY2I3LTQ0YjctYTRlYi02NWNjMzhmMWM1ODIudXNlciJdLCJjbGllbnRfaWQiOiJjbGllbnQiLCJjaWQiOiJjbGllbnQiLCJhenAiOiJjbGllbnQiLCJncmFudF90eXBlIjoiY2xpZW50X2NyZWRlbnRpYWxzIiwicmV2X3NpZyI6ImUyNjU3MDMiLCJpYXQiOjE0OTczNDk5NDksImV4cCI6MTQ5NzM5MzE0OSwiaXNzIjoiaHR0cHM6Ly8xYWI2Nzc1ZC1kMmZhLTQxZDgtOGNkMi1kODFhNjk1NmI1YTQucHJlZGl4LXVhYS5ydW4uYXdzLXVzdzAyLXByLmljZS5wcmVkaXguaW8vb2F1dGgvdG9rZW4iLCJ6aWQiOiIxYWI2Nzc1ZC1kMmZhLTQxZDgtOGNkMi1kODFhNjk1NmI1YTQiLCJhdWQiOlsidGltZXNlcmllcy56b25lcy4xYWQ4ZWYxMi0zY2I3LTQ0YjctYTRlYi02NWNjMzhmMWM1ODIiLCJ1YWEiLCJvcGVuaWQiLCJwcmVkaXgtYXNzZXQuem9uZXMuOTUyM2NhNzYtNDg4Mi00OWRhLWEzYzEtMDFkZTdjODBmYmYzIiwiaW50ZWxsaWdlbnQtbWFwcGluZy56b25lcy41YmJhODJhYi05NjhjLTRiNzctOWEzZS04NWNiYTY3NmE0MzYiLCJjbGllbnQiLCJhbmFseXRpY3Muem9uZXMuMTEwYzExNTctZTNhMi00OGQzLWFmM2QtOTA2ZjEyYjMyZmRlIl19.McwHoobmYdF4vO4UHMZml2V5aiG7R0xhp6XGb_25kjgQRvW5kgEkEcuXHAh8uHJkOcmGkBclLFQObIgLZUmUGccnoeGmVa5HYaHJWh9cuWtXl9HavWNfcsnZ17VTsOcFdm0QaqkhVvFastrr_GsoUMH-_fjmZq24d0W0LNDMGNBYCrHtex5PIiSspO0DQNz3_eBeQ_KMelsycC0g3U_bxVbDgSACp_X98l0xyve5oCGgbSYfxzVe1gY40ifxSdpPFUbPUqjBHoMTi2Ux_zojMuda0PlmV66OUbwsjnbx0doMZ5W0GinPO7eszJUUn946yAMoUHwQHAfGv24PmSIfrg");
xhr.setRequestHeader("predix-zone-id", "110c1157-e3a2-48d3-af3d-906f12b32fde");
xhr.setRequestHeader("cache-control", "no-cache");
xhr.setRequestHeader("postman-token", "b4a1b546-dea6-6b61-a74f-a4fd43e2c214");

xhr.send(data);
