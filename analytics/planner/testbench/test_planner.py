from analytic import PlanningService
import json

if __name__ == '__main__':
    planner = PlanningService()
    result = planner.schedule_task_service('{"start": "1979-08-26 05:00:00", "end": "1979-08-26 10:00:00", "appliance": "iron"}')
    result = json.loads(result)
    result = json.dumps(result, indent=4, sort_keys=True)
    print(result)

