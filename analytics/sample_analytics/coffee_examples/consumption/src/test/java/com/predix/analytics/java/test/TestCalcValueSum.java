package com.predix.analytics.java.test;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import com.predix.analytics.java.ValueSumEntryPoint;

public class TestCalcValueSum {
	@Test
	public void testcalcValueSum() throws IOException, JSONException {
		String inputDataString = getInputDataString();
		ValueSumEntryPoint adder = new ValueSumEntryPoint();

		String result = adder.calcValueSum(inputDataString);
		assertNotNull(result);
		JSONAssert.assertEquals(getExpectedResultString(), result, true);
	}

	private String getInputDataString() throws IOException {
		InputStream inputDataStream = getClass().getClassLoader().getResourceAsStream("analyticInputDataWithTimeseriesData.json");
		return IOUtils.toString(inputDataStream);
	}

	private String getExpectedResultString() throws IOException {
		InputStream expectedOutputDataStream = getClass().getClassLoader().getResourceAsStream("analyticOutputDataWithTimeseriesData.json");
		return IOUtils.toString(expectedOutputDataStream);
	}
}
