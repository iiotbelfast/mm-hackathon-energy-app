package com.predix.analytics.customdto;

public class SumResponse {

	protected double sum;

	@Override public String toString() {
		return "AdderOutput{" + "sumValue=" + sum + '}';
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double value) {
		this.sum = value;
	}
}

