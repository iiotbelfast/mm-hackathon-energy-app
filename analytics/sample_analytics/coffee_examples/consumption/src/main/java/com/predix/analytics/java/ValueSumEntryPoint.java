package com.predix.analytics.java;

import java.io.IOException;
import java.text.DecimalFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.predix.analytics.customdto.SumResponse;


public class ValueSumEntryPoint {

	Logger logger = LoggerFactory.getLogger(ValueSumEntryPoint.class);
	ObjectMapper mapper = new ObjectMapper();

	public String calcValueSum(String jsonStr) throws IOException {

		JsonNode node = mapper.readTree(jsonStr);
		JsonNode dataNode = node.get("data");
		JsonNode timeseriesNode = dataNode.get("time_series");
		JsonNode timestampNode = timeseriesNode.get("time_stamp");
		DecimalFormat sixDP = new DecimalFormat("##.######"); //Six decimal place format
		
		//Initialise Variables
		double calculatedSum = 0.0;
		double j0 = 0.0;
		double j1 = 0.0;
		double jdiff = 0.0;
		
		

		if (timestampNode.isArray()) {
			ArrayNode timestamps = (ArrayNode) timestampNode;

			JsonNode valuesNode = timeseriesNode.get("values");
			ArrayNode values = (ArrayNode) valuesNode;

			for (int i = 0; i < (timestamps.size()-1); i++) {
				
				j0 = (values.get(i).asDouble());
				j1 = (values.get(i+1).asDouble());
				
				if (j1 < j0) {
					jdiff = j0 - j1;
					calculatedSum = (calculatedSum) + (jdiff);
				}
			}
		}
	
		//Format result
		calculatedSum =  Double.valueOf(sixDP.format(calculatedSum)); //Format Mean to 6 DP
		SumResponse output = null;
		output = new SumResponse();
		output.setSum(calculatedSum);

		return mapper.writeValueAsString(output);
	}

}