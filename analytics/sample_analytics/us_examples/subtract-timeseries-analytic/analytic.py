import json


class TimeSeries:

	def __init__(self):
		print("TimeSeries analytic started.")

	def run(self, input_json):
		print("in run()...")
		input = json.loads(input_json)
		ingest_times = input['ingest']['time_stamp']
		ingest_values = input['ingest']['value']
		discharge_times = input['discharge']['time_stamp']
		discharge_values = input['discharge']['value']
		df = {}
		self.__add_to_map(df, ingest_times, ingest_values)
		self.__add_to_map(df, discharge_times, discharge_values)
		result_times = []
		result_values = []
		for ts, value in df.items():
			if len(value) == 2:
				result_times.append(ts)
				result_values.append(value[0] - value[1])
		return json.dumps({
			'head_suction': {
				'time_stamp': result_times,
				'step1': result_values
			}
		})

	@staticmethod
	def __add_to_map(df, timestamps, values):
		for ts, value in zip(timestamps, values):
			if ts not in df:
				df[ts] = []
			df[ts].append(value)

