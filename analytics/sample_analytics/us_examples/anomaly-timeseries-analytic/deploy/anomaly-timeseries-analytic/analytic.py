import json
import pandas as pd


class TimeSeries:

	def __init__(self):
		print("TimeSeries analytic started.")

	def run(self, input_json):
		print("in run()...")
		input = json.loads(input_json)
		threshold = input['threshold']
		df = pd.DataFrame()
		df['value'] = input['data']['value']
		df['time'] = input['data']['time_stamp']
		anomalies = df[df['value'] > threshold]
		return json.dumps({
			'anomaly': {
				'value': anomalies['value'].values.tolist(),
				'time_stamp': anomalies['time'].values.tolist()
			}
		})
