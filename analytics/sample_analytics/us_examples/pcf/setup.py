from setuptools import setup

setup(
	name='pcf',
	version='0.0.1',
	py_modules=['pcf'],
	install_requires=[
		'click',
		'requests'
	],
	entry_points='''
		[console_scripts]
		pcf=pcf:run
	''',
)