# Command line client to assist interaction with Predix APIs.

## Usage:
1. Requirement: Python
1. Install: pip install . --upgrade
1. Configure file pcfconfig.json and type `pcf` to start using the client.