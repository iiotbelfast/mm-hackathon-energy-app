import base64
import click
import json
import os
import requests

CONFIG_FILE = 'pcfconfig.json'
props = None
headers = {}

def save_config():
	json.dump(props, open(CONFIG_FILE, 'w'), indent=2, sort_keys=True)

def pretty_print(text):
	click.echo(json.dumps(json.loads(text), indent=2))

@click.group()
def run():
	global props
	props = json.load(open(CONFIG_FILE))
	if 'uaa_token' in props:
		headers['Authorization'] = 'Bearer ' + props['uaa_token']

@run.command('token')
def token():
	headers = {
		'Authorization': 'Basic ' + base64.b64encode((props['uaa_id'] + ':' + props['uaa_secret']).encode('utf-8')).decode('utf-8'),
		'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
	}
	res = requests.post(props['uaa_url'] + '/oauth/token', headers=headers, data={'grant_type': 'client_credentials'})
	props['uaa_token'] = json.loads(res.text)['access_token']
	save_config()
	pretty_print(res.text)

@run.group('catalog')
def catalog():
	headers['Predix-Zone-Id'] = props['catalog_zone']

@catalog.command('entries')
def list_catalog_entries():
	res = requests.get(props['catalog_url'] + '/api/v1/catalog/analytics', headers=headers)
	pretty_print(res.text)

@catalog.group('entry')
def catalog_entry():
	pass

@catalog_entry.command('target')
@click.option('--name', required=True)
def target_catalog_entry(name):
	res = requests.get(props['catalog_url'] + '/api/v1/catalog/analytics', headers=headers)
	for entry in json.loads(res.text)['analyticCatalogEntries']:
		if entry['name'] == name:
			props['catalog_id'] = entry['id']
			props['catalog_name'] = name
			save_config()
			pretty_print(json.dumps(entry))
			click.echo("id: {}".format(entry['id']))
			break
	else:
		pretty_print(res.text)
		click.echo("No such catalog entry.")

@catalog_entry.command('create')
@click.option('--name', required=True)
@click.option('--lang', default='Java', show_default=True)
def create_catalog_entry(name, lang):
	headers['Content-Type'] = 'application/json'
	res = requests.post(props['catalog_url'] + '/api/v1/catalog/analytics', headers=headers, data=json.dumps({
		'name': name,
		'supportedLanguage': lang,
		'version': 'v1',
		'author': 'FPT'
	}))
	props['catalog_id'] = json.loads(res.text)['id']
	props['catalog_name'] = name
	save_config()
	pretty_print(res.text)

@catalog_entry.command('delete')
def delete_catalog_entry():
	res = requests.delete(props['catalog_url'] + '/api/v1/catalog/analytics/' + props['catalog_id'], headers=headers)
	assert res.status_code == 204

@catalog.command('artifacts')
def list_catalog_artifacts():
	res = requests.get(props['catalog_url'] + '/api/v1/catalog/analytics/' + props['catalog_id'] + '/artifacts', headers=headers)
	pretty_print(res.text)

@catalog.group('artifact')
def catalog_artifact():
	pass

@catalog_artifact.command('upload')
@click.option('--file', required=True)
@click.option('--type', required=True)
def upload_catalog_artifact(file, type):
	res = requests.get(props['catalog_url'] + '/api/v1/catalog/analytics/' + props['catalog_id'] + '/artifacts', headers=headers)
	artifact_id = None
	for artifact in json.loads(res.text)['artifacts']:
		if artifact['type'] == type:
			artifact_id = artifact['id']
	headers['Content-Type'] = 'multipart/form-data'
	files = {
		'file': open(file, 'rb'),
		'catalogEntryId': props['catalog_id'],
		'type': type
	}
	if artifact_id is None:
		res = requests.post(props['catalog_url'] + '/api/v1/catalog/artifacts', headers=headers, files=files)
	else:
		res = requests.put(props['catalog_url'] + '/api/v1/catalog/artifacts/' + artifact_id, headers=headers, files=files)
	pretty_print(res.text)

@catalog.group('validation')
def catalog_validation():
	pass

@catalog_validation.command('async')
@click.option('--file', required=True)
def request_catalog_validation(file):
	headers['Content-Type'] = 'application/json'
	res = requests.post(props['catalog_url'] + '/api/v1/catalog/analytics/' + props['catalog_id'] + '/validation', headers=headers, data=open(file).read())
	props['catalog_validation_id'] = json.loads(res.text)['validationRequestId']
	save_config()
	pretty_print(res.text)

@catalog_validation.command('status')
def check_catalog_validation():
	res = requests.get(props['catalog_url'] + '/api/v1/catalog/analytics/' + props['catalog_id'] + '/validation/' + props['catalog_validation_id'], headers=headers)
	if json.loads(res.text)['status'] == 'PROCESSING':
		click.echo('PROCESSING')
	else:
		pretty_print(res.text)

@catalog.group('execution')
def catalog_execution():
	pass

@catalog_execution.command('now')
@click.option('--file', required=True)
def call_catalog_execution(file):
	headers['Content-Type'] = 'application/json'
	res = requests.post(props['catalog_url'] + '/api/v1/catalog/analytics/' + props['catalog_id'] + '/execution', headers=headers, data=open(file).read())
	pretty_print(res.text)

@catalog.command('log')
def catalog_log():
	res = requests.get(props['catalog_url'] + '/api/v1/catalog/analytics/' + props['catalog_id'] + '/logs', headers=headers)
	click.echo(res.text)

@run.group('runtime')
def runtime():
	headers['Predix-Zone-Id'] = props['runtime_zone']

@runtime.command('config')
def configure_runtime_service():
	click.echo(json.dumps({
		'trustedIssuerIds': ["{}/oauth/token".format(props['uaa_url'])],
		'dependentServices': {
			'predixAssetZoneId': props['asset_zone'],
			'predixTimeseriesZoneId': props['timeseries_zone'],
			'predixAnalyticsCatalogZoneId': props['catalog_zone']
		},
		'trustedClientCredential': {
			'clientId': props['uaa_id'],
			'clientSecret': props['uaa_secret']
		}
	}))

@runtime.command('entries')
def list_runtime_entries():
	res = requests.get(props['runtime_url_config'] + '/api/v2/config/orchestrations', headers=headers)
	pretty_print(res.text)

@runtime.group('entry')
def runtime_entry():
	pass

@runtime_entry.command('target')
@click.option('--name', required=True)
def target_runtime_entry(name):
	res = requests.get(props['runtime_url_config'] + '/api/v2/config/orchestrations', headers=headers)
	for entry in json.loads(res.text)['orchestrationEntries']:
		if entry['name'] == name:
			props['runtime_id'] = entry['id']
			props['runtime_name'] = name
			props['runtime_artifact_id'] = ''
			save_config()
			pretty_print(json.dumps(entry))
			click.echo("id: {}".format(entry['id']))
			break
	else:
		pretty_print(res.text)
		click.echo("No such runtime entry.")

@runtime_entry.command('create')
@click.option('--name', required=True)
def create_runtime_entry(name):
	headers['Content-Type'] = 'application/json'
	res = requests.post(props['runtime_url_config'] + '/api/v2/config/orchestrations', headers=headers, data=json.dumps({
		'name': name,
		'author': 'FPT',
		'description': name
	}))
	props['runtime_id'] = json.loads(res.text)['id']
	props['runtime_name'] = name
	save_config()
	pretty_print(res.text)

@runtime_entry.command('delete')
def delete_runtime_entry():
	# TODO
	pass

@runtime.command('artifacts')
def list_runtime_artifacts():
	res = requests.get(props['runtime_url_config'] + '/api/v2/config/orchestrations/' + props['runtime_id'] + '/artifacts', headers=headers)
	pretty_print(res.text)

@runtime.group('artifact')
def runtime_artifact():
	pass

@runtime_artifact.command('target')
@click.option('--file', required=True)
def target_runtime_artifact(file):
	filename = os.path.basename(file)
	res = requests.get(props['runtime_url_config'] + '/api/v2/config/orchestrations/' + props['runtime_id'] + '/artifacts', headers=headers)
	for artifact in json.loads(res.text)['orchestrationArtifacts']:
		if artifact['filename'] == filename:
			props['runtime_artifact_id'] = artifact['id']
			save_config()
			pretty_print(json.dumps(artifact))
			click.echo("id: {}".format(artifact['id']))
			break
	else:
		pretty_print(res.text)
		click.echo("No such runtime artifact.")

@runtime_artifact.command('upload')
@click.option('--file', required=True)
@click.option('--type', required=True)
@click.option('--name', default=None, show_default=True)
def upload_runtime_artifact(file, type, name):
	headers['Content-Type'] = 'multipart/form-data'
	files = {
		'file': open(file, 'rb'),
		'orchestrationEntryId': props['runtime_id'],
		'type': type
	}
	if name is not None:
		files['name'] = name
	res = requests.post(props['runtime_url_config'] + '/api/v2/config/orchestrations/artifacts', headers=headers, files=files)
	props['runtime_artifact_id'] = json.loads(res.text)['id']
	save_config()
	pretty_print(res.text)

@runtime_artifact.command('delete')
def delete_runtime_artifact():
	res = requests.delete(props['runtime_url_config'] + '/api/v2/config/orchestrations/artifacts/' + props['runtime_artifact_id'], headers=headers)
	assert res.status_code == 204

@runtime.group('execution')
def runtime_execution():
	pass

@runtime_execution.command('async')
@click.option('--file', required=True)
def request_runtime_execution(file):
	headers['Content-Type'] = 'application/json'
	data = json.load(open(file))
	if 'runtime_id' in props and props['runtime_id'] != '':
		data['orchestrationConfigurationId'] = props['runtime_id']
	res = requests.post(props['runtime_url_execution'] + '/api/v2/execution/async', headers=headers, data=json.dumps(data))
	props['runtime_request_id'] = json.loads(res.text)['orchestrationRequestId']
	save_config()
	pretty_print(res.text)

@runtime_execution.command('status')
def check_runtime_execution():
	res = requests.get(props['runtime_url_monitoring'] + '/api/v1/monitoring/orchestrations/' + props['runtime_request_id'], headers=headers)
	if json.loads(res.text)['status'] == 'PROCESSING':
		click.echo('PROCESSING')
	else:
		pretty_print(res.text)

if __name__ == '__main__':
	run()
