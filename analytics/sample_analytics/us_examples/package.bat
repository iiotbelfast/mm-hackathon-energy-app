@echo off
rem guard against empty input
if [%1] == [] exit /b
cd %1
zip deploy/%1.zip *.py *.json
cd ..
