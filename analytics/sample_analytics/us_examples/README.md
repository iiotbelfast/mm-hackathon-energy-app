# Sample Time-Series Analytics


## Purpose: This project demonstrates Analyics with Time-Series.


### High-level architecture:

Theoretically our analytic can return whatever JSON string. To inform Runtime service of the JSON format, we need a template file.

To input data according to the template, we need a port-to-field mapping file, which provides the actual value or an alias to an external data source (e.g. Time-Series).

Finally when running the Orchestration Entry, we provide a JSON file that maps the alias to the actual data source (e.g. tag name).


### Pre-conditions:

1. Basically you only need a REST client to interact with Analytics services. For reference, I wrote my own implementation in Python: pcf. To install it:

	`pip install . --upgrade`

1. Some batch/shell/GNU familiarity is advantageous.


### Steps to do:

1. Configure pcfconfig.json with: asset_zone, catalog_zone, runtime_zone, timeseries_zone, uaa_id, uaa_secret, uaa_url.

1. Get a token:

	`pcf token`

1. Create an Analytic Entry:

	`pcf catalog entry create --name subtract-timeseries-analytic-sulzer`
	pcf catalog entry create --name anomaly-timeseries-analytic-sulzer
pcf catalog entry create --name cavitation-analytic --lang=Java

1. Zip the Python Analytic (the batch script can be easily translated to shell):

	`package subtract-timeseries-analytic`

1. Upload the zip file to Analytic service:

pcf catalog artifact upload --file efficiency-timeseries-analytic/deploy/efficiency-analytics-1.0.0.jar --type executable

pcf catalog artifact upload --file cavitation-timeseries-analytic/deploy/cavitation-analytics-1.0.0.jar --type executable
	`pcf catalog artifact upload --file subtract-timeseries-analytic/deploy/subtract-timeseries-analytic.zip --type executable`
    pcf catalog artifact upload --file anomaly-timeseries-analytic/deploy/anomaly-timeseries-analytic.zip --type executable`

1. Validate the Analyic (by deploying it to Cloud Foundry):

	`pcf catalog validation async --file anomaly-timeseries-analytic/deploy/input.json`
pcf catalog validation async --file efficiency-timeseries-analytic/input.json
pcf catalog validation async --file cavitation-timeseries-analytic/input.json
1. Wait for status "COMPLETED" (quite a long time):

	`pcf catalog validation status`

1. Test the execution synchronously:

	`pcf catalog execution now --file anomaly-timeseries-analytic/deploy/input.json`
pcf catalog execution now --file efficiency-timeseries-analytic/input.json

1. Create an Orchestration Entry:

	`pcf runtime entry create --name subtract-timeseries-runtime`
pcf runtime entry create --name cavitation-runtime
1. Upload the workflow configuration (you might want to update the file with the Analytic Catalog Entry ID, in the future my client will update it automatically):

	`pcf runtime artifact upload --file subtract-timeseries-analytic/deploy/workflow.bpmn.xml --type bpmn`

pcf runtime artifact upload --file cavitation-timeseries-analytic/orchestration-workflow.xml --type bpmn
pcf runtime artifact target --file cavitation-timeseries-analytic/orchestration-workflow.xml
    pcf runtime artifact target --file anomaly-timeseries-analytic/deploy/workflow.bpmn.xml
    pcf runtime artifact delete
1. Upload the template configuration:

	`pcf catalog artifact upload --file subtract-timeseries-analytic/deploy/template.json --type template`
pcf catalog artifact upload --file cavitation-timeseries-analytic/template.json --type template
pcf catalog artifact target --file cavitation-timeseries-analytic/template.json --type template

    pcf catalog artifact target --file subtract-timeseries-analytic/deploy/template.json
    pcf catalog artifact upload --file anomaly-timeseries-analytic/deploy/template.json --type template
1. Upload the port-to-field configuration:

	`pcf runtime artifact upload --file subtract-timeseries-analytic/deploy/port-to-field.json --type portToFieldMap --name sid-10001`


	pcf runtime artifact upload --file anomaly-timeseries-analytic/deploy/port-to-field.json --type portToFieldMap --name sid-10001

    pcf runtime artifact target --file anomaly-timeseries-analytic/deploy/port-to-field.json
    pcf runtime artifact delete

		pcf runtime artifact upload --file cavitation-timeseries-analytic/port-to-field.json --type portToFieldMap --name sid-10001

pcf runtime artifact target --file cavitation-timeseries-analytic/port-to-field.json
		pcf runtime artifact upload --file cavitation-timeseries-analytic/port-to-field.json --type portToFieldMap --name EfficiencyCalculatorOrchestration
1. Run the Orchestration Entry:

	`pcf runtime execution async --file subtract-timeseries-analytic/deploy/runtime-execution.json`
	pcf runtime execution async --file anomaly-timeseries-analytic/deploy/runtime-execution.json
pcf runtime execution async --file cavitation-timeseries-analytic/runtime-execution.json
1. Wait for status "COMPLETED":

	`pcf runtime execution status`

1. Query the result in Time-Series.

pcf catalog artifact upload --file efficiency-timeseries-analytic/efficiency-calculator-template.json --type template
pcf runtime artifact upload --file efficiency-timeseries-analytic/orchestration-workflow.xml --type bpmn
pcf runtime artifact upload --file efficiency-timeseries-analytic/port-to-field-map-for-efficiency.json --type portToFieldMap --name EfficiencyCalculatorOrchestration
pcf runtime artifact target --file efficiency-timeseries-analytic/port-to-field-map-for-efficiency.json
pcf runtime artifact delete
pcf runtime execution async --file efficiency-timeseries-analytic/runtime-execution.json
