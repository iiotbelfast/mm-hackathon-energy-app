# README #
---
Repository for Deloitte's Predix Hackathon Energy App, first created at GE Digital's Minds+Machines Hackathon in Berlin, 12th June 2017.
+ Hackathon Team:
	+ Steven Bailie - sbailie@deloitte.co.uk
	+ Kaloyan Pashov - kpashov@deloitte.co.uk
	+ Mayank Khare - mayankkhare@deloitte.co.uk
	+ Luke Teacy - lteacy@deloitte.co.uk
	+ Vysak Venkateswaran - vvenkateswaran@deloitte.com
	+ Fred Burder - fburder@deloitte.co.uk

# Access Information #
---
+ Login: https://esch-dev-ui.run.aws-usw02-pr.ice.predix.io
+ Username: user
+ Password: pass

# App Description
---
+ This app contains two views.
	+ The "Supply" view allows you to explore the breakdown of energy sources for the UK national grid 2016. Using the range picker, and it's presets, you can explore the area chart and see the demand spiking every day; the trends throughout the year, and trends such as the unusual dip around December 25th.
	+ The "Scheduler" view allows you to choose an appliance and a range of time in which you want it to run, and using both the average weekly supply forecast and the particular appliance demand profile; the app will recommend the optimum time to begin your appliance.  *(Use the toggle to have the range begin now, and you then only pick the end point of the range).* This result is then presented as a timeseries chart.

# Service Details #
---
+ These are the service details for all of the services that the Hackathon Energy app is dependent on, as of 11/10/2017.
+ The original services were created in a dedicated M+M Hackathon space which has long since depreciated.
+ These new services are created in the "DDBuckley" space of the "Deloitte_Consulting_LLP_Basic" organisation by Deloitte UK.

### Service Details
| Service Name | Service Zone ID/Guid | Service Type | Description | URL |
| --- | --- | --- | --- | --- |
| esch-dev-uaa | 7e05ce7a-ff17-4744-8fee-849007b1a2e8 | UAA | User Management for the App | https://uaa-dashboard.run.aws-usw02-pr.ice.predix.io/#/login/7e05ce7a-ff17-4744-8fee-849007b1a2e8 |
| esch-dev-timeseries | d8b4fbb3-7a6e-4b83-9c93-d00bfc5b93bf | TimeSeries DB | Database for storing timeseries data | N/A |
| esch-dev-asset | 8e4a8ae2-413b-4511-9302-27e961ce0d45 | Asset Store | Asset data store | N/A |
| esch-dev-analytics | b27c247e-9398-4341-896b-de29bb82dbe8 | Analytics Framework | Analytic catalog and runtime | https://esch-dev-analytics-ui.predix-analytics-ui.run.aws-usw02-pr.ice.predix.io/ |


### UAA Roles and Credientals
| Role | Username | Password |
| --- | --- | --- |
| Admin | Admin | SecurePassword |
| Client | esch-client | SecurePassword |
| User | user | pass |

### Analytics Details
| Type | Name | ID | Deployment Request | Corresponding Analytic |
| --- | --- | --- | --- | --- |
| Analytic | planning-service | e5cabb6f-a9d3-4032-8da1-0f0ef398c4c1 | 472768be-23c9-4474-a02f-ff797e3568cc | N/A |
| Analytic | planning-service2 | ae64eb02-b288-4219-927a-2bb23d88588e | c25334c1-b08f-49f3-94fe-8b7ffd39cfd5 | N/A |
| Orchestration | --- | --- | --- | --- |
| Orchestration | --- | --- | --- | --- |


# How to set up #
---
### Get the source code
Make a directory for your project. Clone or download and extract the starter in that directory.
### Install tools
If you don't have them already, you'll need node, bower and gulp to be installed globally on your machine.
1. Install [node](https://nodejs.org/en/download/). This includes npm - the node package manager.
2. Install [bower](https://bower.io/) globally `npm install bower -g`
3. Install [gulp](http://gulpjs.com/) globally `npm install gulp-cli -g`
### Install the dependencies
Change directory into the new project you just cloned, then install dependencies.
```
npm install
bower install
```
### Running the app locally
The default gulp task will start a local web server. Just run this command:
```
gulp
```
Browse to https://localhost:5000.
### Running in Predix Cloud
With a few commands you can build a distribution version of the app, and deploy it to the cloud.
### Create a distribution version
Use gulp to create a distribution version of your app, which contains vulcanized files for more efficient serving.
You will need to run this command every time before you deploy to the Cloud.
```
gulp dist
```
### Push to the cloud
Pushing (deploying) to a cloud environment requires knowledge of the commands involved and a valid user account with the environment. GE uses Cloud Foundry for its cloud platform. For information on Cloud Foundry, refer to this [link](http://docs.cloudfoundry.org/cf-cli/index.html).
The simplest way to push to a cloud environment is by modifying the default manifest file (manifest.yml) and using the **cf push** command.
